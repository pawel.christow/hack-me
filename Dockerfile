FROM php:7.0-apache
MAINTAINER secdim.com
WORKDIR /var/www/html/
RUN   apt-get update && apt-get -y install keychain

COPY ./src/ /var/www/html/
RUN chmod -R 0555 /var/www/html/

RUN useradd -m user \
    && mkdir -p /home/user/.ssh \
    && ssh-keygen -t rsa -f /home/user/.ssh/id_rsa -q -P "" \
    && gunzip -c admin/db_dump.sql.gz > /home/user/db_dump.sql \
    && rm -rf /var/www/html/admin/db_dump.sql.gz

EXPOSE 80    

